(defpackage :wasteless.user-system
  (:use :cl :asdf))
(in-package :wasteless.user-system)

(defsystem :wasteless.user
  :serial t
  :depends-on (#:postmodern
               #:wasteless.common
               #:cl-password)
  :components ((:file "package")
               (:file "user")))
