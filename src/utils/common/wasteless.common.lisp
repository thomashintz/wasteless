(in-package #:wasteless.common)

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defun mapa-b (fn a b &optional (step 1))
  "map starting at a and going to b by fn.
   > (mapa-b #'1+ -2 0 .5)
     (-1 -0.5 0.0 0.5 1.0)"
  (do ((i a (+ i step))
       (result nil))
      ((> i b) (nreverse result))
    (push (funcall fn i) result)))

(defun map0-n (fn n)
  "map from 0 to n. (map0-n #'1+ 5)"
  (mapa-b fn 0 n))

; from Paul Graham's On Lisp
(defmacro do-tuples/o (parms source &body body)
  "Iterate over a list by tuples. /o for 'open'
   (do-tuples/o (x y) '(a b c d)
     (princ (list x y)))
   => (A B)(B C)(C D)"
  (if parms
      (let ((src (gensym)))
        `(prog ((,src ,source))
            (mapc #'(lambda ,parms ,@body)
                  ,@(map0-n #'(lambda (n)
                                `(nthcdr ,n ,src))
                            (1- (length parms))))))))

(defun between (val start end)
  "Is val between start and end? start is inclusive. end is exclusive."
  (and (>= val start) (< val end)))

(defun ++ (&rest strings)
  "Concatenate strings together."
  (apply #'concatenate 'string strings))

(defun find-src-path ()
  (let ((path-file "~/.wasteless-src"))
    (if (probe-file path-file)
        (with-open-file (f path-file)
          (let ((src-path (read f)))
            (if (directory src-path)
                src-path
                (error (++ "cannot find wasteless src path: " src-path)))))
        (error "cannot find wasteless src path file"))))

(defparameter *src-path* (find-src-path))
(defun wasteless-src-path () *src-path*)

(setq cl-config:*defaults-config-file* (++ (wasteless-src-path) "/etc/defaults/web.lconf"))
(setq cl-config:*overrides-config-file* (++ (wasteless-src-path) "/etc/web.lconf"))

(defun load-config () (cl-config:load-config))

(defun conf (setting) (cl-config:conf setting))

(load-config)

(defmacro w/c (&body body)
  "Do query with foods database connection."
  (with-gensyms (transaction)
    `(postmodern:with-connection
         '(,(conf 'db-database) ,(conf 'db-user) ,(conf 'db-password) ,(conf 'db-host)
           :pooled-p t)
       (postmodern:with-transaction (,transaction)
         ,@body))))

(defmacro with-shadow ((fname fun) &body body)
  "Shadow the function named fname with fun
   Any call to fname within body will use fun, instead of the default function for fname.
   This macro is intentionally unhygienic:
   fun-orig is the anaphor, and can be used in body to access the shadowed function"
  `(let ((fun-orig))
     (cond ((fboundp ',fname)
            (setf fun-orig (symbol-function ',fname))
            (setf (symbol-function ',fname) ,fun)
            (unwind-protect (progn ,@body)
              (setf (symbol-function ',fname) fun-orig)))
           (t
            (setf (symbol-function ',fname) ,fun)
            (unwind-protect (progn ,@body)
              (fmakunbound ',fname))))))

(defparameter *urls*
  '(app ""
    (user "user"
     (sign-in "sign-in")
     (sign-out "sign-out")
     (sign-in-trampoline "sign-in-trampoline")
     (user-pref "pref"
      (email-digest "email-digest")
      (tracking "tracking")
      (disable-email-digest-by-id "disable-email-digest-by-id")))
    (food "food"
     (add-food "add")
     (remove-food "remove")
     (rename-food "rename"))
    (demo "try")
    (css "css"
     (css-main-root "main"
      (css-main "")))))

(defparameter *url-table* (make-hash-table))

(defun add-urls-to-table (base tree hash-table)
  (let ((key (car tree)) (path (++ base (cadr tree))))
    ; don't add slash on leaf
    (setf (gethash (intern (symbol-name key) 'wasteless.common) hash-table)
          (++ path (if (cddr tree) "/" "")))
    (if (cddr tree)
        (dolist (branch (cddr tree))
          (add-urls-to-table (++ path "/") branch hash-table)))))

(defun populate-url-table () (add-urls-to-table "" *urls* *url-table*))
(populate-url-table)

(defun url (key)
  (gethash (intern (symbol-name key) 'wasteless.common) *url-table*))

(defun add-urls (base tree)
   (add-urls-to-table base tree *url-table*))
