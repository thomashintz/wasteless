(in-package :wasteless.web)

(load-config)
(log:config :error)
(log:config :daily (conf 'log-path))

;;; WEB SERVER ;;;
(defvar *main-acceptor* nil)

(defparameter *invoke-debugger-on-error* (not (conf 'production-system)))

(when (conf 'production-system)
  (setf *show-lisp-errors-p* nil
        *show-lisp-backtraces-p* nil))

(setf *catch-errors-p* (conf 'production-system))

(setf (html-mode) :html5
      *database-connection-spec* `(,(conf 'db-database) ,(conf 'db-user) ,(conf 'db-password)
                                    ,(conf 'db-host) :pooled-p t))

(defclass wasteless-session (pg-session)
  ((user-id :col-type integer :initarg :user-id :accessor session-user-id)
   (anonymous-account :col-type boolean :initarg :anonymous-account
                      :accessor session-anonymous-account)
   (tracking :col-type boolean :initarg :tracking :accessor session-tracking))
  (:metaclass dao-class))

(setf *session-type* 'wasteless-session
      *pg-session-max-time* (* 60 60 24 7 2)) ; two weeks

(when (conf 'production-system)
  (defmethod maybe-invoke-debugger ((c condition))
    (unless *invoke-debugger-on-error*
      (log:error (trivial-backtrace:print-backtrace c :output nil))
      (setf (return-code*) +http-internal-server-error+)
      (abort-request-handler
       (with-html-output-to-string
           (*standard-output* nil :prologue t)
         (:html
          (:head
           (:meta :name "viewport" :content "width=device-width"))
          (:body
           (:h1 "Internal server error"))))))))

(defclass wasteless-acceptor (easy-acceptor) ())

(defmethod acceptor-dispatch-request ((wasteless-acceptor wasteless-acceptor) request)
  (log:info "***:uri ~A ***:ip ~A ***:ua ~A"
            (request-uri* request) (real-remote-addr request) (user-agent request))
  (call-next-method))

(defun start-server ()
  (setf *main-acceptor* (start (make-instance 'wasteless-acceptor :port (conf 'httpd-port))))
  (setf (acceptor-access-log-destination *main-acceptor*) nil))

(defun stop-server () (stop *main-acceptor*))

;;; DATABASE SETUP ;;;
(defun ensure-food-table-exists ()
  (unless (table-exists-p "food")
      (execute (dao-table-definition 'food))))

(defun ensure-user-table-exists ()
  (unless (table-exists-p 'user-accounts)
    (execute (dao-table-definition 'user))))

(defun ensure-session-table-exists ()
  (unless (table-exists-p 'wasteless-session)
    (execute (dao-table-definition 'wasteless-session))))

(defun ensure-tables-exist ()
  (ensure-food-table-exists)
  (ensure-session-table-exists)
  (ensure-user-table-exists))
(w/c (ensure-tables-exist))

;;; UTILITIES ;;;
(defun empty-p (string) (string= string ""))

(defun sessionless-urls ()
  `(,(url 'sign-in) ,(url 'sign-in-trampoline) ,(url 'sign-out)
     ,(url 'disable-email-digest-by-id) ,(url 'css-main)
     ,(url 'demo)))


;;; REQUEST GENERIC METHODS ;;;
(defun uri-root (uri)
  (values (subseq uri 0 (search "?" uri))
          (if (search "?" uri)
              (subseq uri (search "?" uri))
              "")))

(define-condition wasteless-redirect (condition)
    ((location :initarg :location :reader wasteless-redirect-location)))

(defun redirect (target)
  "See define-easy-handler/db for details."
  (if *session*
      (signal 'wasteless-redirect :location target)
      (hunchentoot:redirect target)))

(defun create-user-session (user-id &key (anonymous-account nil))
  (pg-start-session :user-id user-id :anonymous-account anonymous-account))

(defmethod handle-request :before ((acceptor acceptor) (request request))
  (when (and (null *session*)
             (not (member (uri-root (request-uri request)) (sessionless-urls)
                          :test #'string=)))
    (redirect (url 'sign-in))))

;;; STANDARD PAGE TEMPLATE ;;;
(defmacro standard-page (&body body)
  `(with-html-output-to-string
       (*standard-output* nil :prologue t)
     (:html
      (:head
       (:meta :name "viewport" :content "width=device-width")
       (:link :type "text/css" :rel "stylesheet"
              :href (url 'css-main))
       (when (or (null *session*) (pg-session-value 'tracking))
         (htm (:script :type "text/javascript"
                       "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-15600702-6', 'wasteless.io');
  ga('send', 'pageview');"))))
      (:body
       (:section
        :class "navbar"
        (:a :href "http://wasteless.io" "wasteless.io") " | "
        (if (or (null *session*) (string= (request-uri *request*) (url 'sign-out)))
            (htm (:a :href (url 'sign-in) "sign in"))
            (htm (:a :href (url 'app) "my foods")
                 " | "
                 (:a :href (url 'sign-out) "sign out"))))
       ,@body
       (:section :class "footerbar")))))

(defmacro hidden-submit (submit-value)
  `(htm (:input :type "submit" :value ,submit-value
                :style (conc "position: absolute; left: -9999px;width: 1px; height: 1px;"))))



;;; WEB ;;;
(define-condition user-id-not-found-error (error)
  ((user-id-not-found-error-id :initarg :user-id :reader user-id-not-found-error-id)))

(define-condition email-not-found-error (error)
  ((email-not-found-error-email :initarg :email :reader email-not-found-error-email)))

(defun get-user-email-by-id (id)
  (aif (query (:select 'email :from 'user-accounts :where (:= 'user-id '$1)) id :single)
       it
       (error 'user-id-not-found-error :user-id id)))

(defun get-user-id-by-email (email)
  (aif (query (:select 'user-id :from 'user-accounts :where (:= 'email '$1)) email
              :single)
       it
       (error 'email-not-found-error :email email)))

(defun current-user ()
  (get-dao 'user (get-user-email-by-id (pg-session-value 'user-id))
           (pg-session-value 'user-id)))

(defun current-foods ()
  (select-dao 'food (:= 'user-id (pg-session-value 'user-id)) 'expires))

(define-condition invalid-user-credentials-error (error)
    ((invalid-user-credentials-email :initarg :email
                                     :reader invalid-user-credentials-email)))

(defun verify-user-credentials (email password)
  (if (and (account-exists-p email)
           (check-password password
                           (password-string->plist
                            (user-password (get-dao 'user email
                                                    (get-user-id-by-email email))))))
      t
      (progn ; simulate a password check
        (hash-password "fooey" :scrypt)
        (error 'invalid-user-credentials-error :email email))))

(defmacro sign-in-form (&key name type submit-text autofocus)
  `(htm
    (:form :name ,name :action (url 'sign-in-trampoline) :method "post"
            (:input :type "hidden" :name "type" :value ,type)
            (:p (:input :type "email" :placeholder "email" :name "email"
                        :autofocus ,autofocus))
            (:p (:input :type "password" :placeholder "password" :name "password"))
            (:p (:input :type "submit" :value ,submit-text)))))

(defmacro define-easy-handler/db (description lambda-list &body body)
  "Wraps define-easy-handler in with-connection and with-transaction.

   To ensure that the transaction is committed and the connection
   is closed we have provided our own redirection handling.
   Hunchentoot does a throw when its redirect method is called which
   would prevent the db from getting the changes it should and causes
   threading issues with the connections in pg-session. So instead we
   shadow hunchentoot's redirect with our own which sends a signal on
   redirect which gets caught and handled gracefully with regards to
   the db."
  (with-gensyms (transaction)
    `(define-easy-handler ,description ,lambda-list
       (let*
           ((redirect-to nil)
            (result
             (with-connection
                 `(,(conf 'db-database) ,(conf 'db-user) ,(conf 'db-password) ,(conf 'db-host)
                    :pooled-p t)
               (with-transaction (,transaction)
                 (handler-case
                     (progn ,@body)
                   (wasteless-redirect (r)
                     (setf redirect-to (wasteless-redirect-location r))))))))
         (when redirect-to
           (hunchentoot:redirect redirect-to))
         result))))

(define-easy-handler/db
    (sign-in-handler :uri (url 'sign-in))
    (signed-out hide-sign-in error-message)
  (standard-page
    (:p (:a :href (url 'demo) "try wasteless without an account"))
    (when signed-out (htm (:p "you have been signed out")))
    (when error-message (htm (:p (fmt "~A" error-message))))
    (unless hide-sign-in
      (sign-in-form :name "sign-in" :type "sign-in" :submit-text "sign in" :autofocus t))
    (sign-in-form :name "new-account" :type "new-account" :submit-text "create account")))
(defun valid-email-p (s)
  (= (length (cl-ppcre:split "@" s)) 2))

(defun construct-url (base &rest parts)
  (with-output-to-string (s)
    (princ base s)
    (princ "?" s)
    (loop for p in parts
       for i = 0 then (+ i 1)
       collect
         (if (evenp i)
             (princ (string-downcase (princ-to-string p)) s)
             (princ (++ "=" (url-encode (princ-to-string p)) "&") s)))))

(defun sign-in-page-error-url (message)
  (construct-url (url 'sign-in) :error-message message))

(define-easy-handler/db
    (sign-in-trampoline :uri (url 'sign-in-trampoline)) (email password type)
  (unless (valid-email-p email) (redirect (sign-in-page-error-url "invalid email format")))
  (if (and type (string= type "sign-in"))
      (handler-case (verify-user-credentials email password)
        (invalid-user-credentials-error ()
          (redirect (sign-in-page-error-url "unknown account or invalid password"))))
      (handler-case (add-user-account email password :user-id (pg-session-value 'user-id))
        (user-account-error (c)
          (redirect (construct-url (url 'sign-in)
                                   :error-message (user-account-error-message c))))))
  (create-user-session
   (or (pg-session-value 'user-id)
       (query (:select 'user-id :from 'user-accounts :where (:= 'email '$1)) email
              :single)))
  (setf (pg-session-value 'anonymous-account) nil)
  (redirect (url 'app)))

(define-easy-handler/db
    (sign-out-handler :uri (url 'sign-out)) ()
  (unless (null *session*) (pg-remove-session *session*))
  (redirect (conc (url 'sign-in) "?signed-out=true")))

;;; MAIN APP PAGES ;;;
(define-easy-handler/db
  (main :uri (url 'app))
    (missing-name invalid-expires name-too-long filter
     undo undo-name (undo-expires :parameter-type 'integer) undo-orig-name undo-cur-name)
  ; Places a header at the top of a time span, like 'expired' or 'months'.
  (macrolet ((group-headers (food-var previous-expiration-var groups)
                                        ; Creates a test for the cond that checks to see if a
                                        ; new header should be added.
               (macrolet ((expiration-range (start end)
                            ``(and (<= (food-expires-in-days ,food-var) ,,end)
                                   (> (food-expires-in-days ,food-var) ,,start)
                                   (<= ,previous-expiration-var ,,start))))
                 `(cond ,@(loop for (start end label) in groups collect
                               `(,(expiration-range start end)
                                  (htm (:div :class "time-header" ,label))))))))
    (standard-page
      (when (pg-session-value 'anonymous-account)
        (htm (:p "save your changes - "
                 (:a :href (conc (url 'sign-in) "?hide-sign-in=true")
                     "create an account"))))
      (when missing-name (htm (:p "missing food name")))
      (when invalid-expires (htm (:p "invalid food expiration")))
      (when name-too-long (htm (:p "name must be less than 150 characters")))
      (when (string= undo "add")
        (htm (:a :href (escape-string
                        (construct-url (url 'add-food) :name undo-name :expires undo-expires))
                 "undo")))
      (when (string= undo "rename")
        (htm (:a :href (construct-url
                        (url 'rename-food)
                        :name undo-orig-name :old-name undo-cur-name
                        :numeric-expires undo-expires
                        :expires-in (timestamp->days undo-expires)
                        :initial-expires-text (timestamp->days undo-expires))
                 "undo")))
      (:section :class "add-food-section"
                (:form :name "add-food" :action (url 'add-food) :method "post"
                       (:input :type "text" :name "name" :placeholder "food name"
                               :autofocus t :style "width: 140px" :autocomplete "off")
                       (:input :type "text" :name "expires" :placeholder "expires"
                               :style "width: 100px" :autocomplete "off")
                       (:input :type "submit" :value "add food"))
                (:form :name "filter" :action (url 'app) :method "post"
                       (:input :type "text" :name "filter" :placeholder "filter"
                               :id "filter")
                       (:input :type "submit" :value "search")
                       (:span "&nbsp;&nbsp;")
                       (:a :href (url 'app) "reset")))
      (:section
       :id "food-list-container"
       (loop
          with food-list = (if filter
                               (remove-if (complement
                                           #'(lambda (e) (search (string-downcase filter)
                                                            (string-downcase (food-name e)))))
                                          (current-foods))
                               (current-foods))
          for f in food-list
          for i = 0 then (+ i 1)
          ; Keep track of the previous expiration so we know when to
          ; add in a new group header.
          for previous-expiration = -1000 then
            (food-expires-in-days (nth (- i 1) food-list))
          do
            (htm
             (group-headers f previous-expiration
                            ((-1000 0 "expired")
                             (0 6 "days")
                             (6 30 "weeks")
                             (30 340 "months")
                             (340 10000 "years")))
             (:div :class "food-edit-row"
                   (:form :name "rename-food" :action (url 'rename-food) :method "post"
                          (:input :type "hidden" :value (escape-string (food-name f))
                                  :name "old-name")
                          (:input :type "hidden" :value (food-expires f)
                                  :name "numeric-expires")
                          (:input :type "hidden" :value (escape-string (food-days-natural f))
                                  :name "initial-expires-text")
                          (:input :type "text" :name "name" :placeholder "food name"
                                  :class "food-edit-input food-name-input"
                                  :value (escape-string (food-name f)) :autocomplete "off")
                          (:input :type "text" :name "expires-in"
                                  :placeholder "expires"
                                  :value (escape-string (food-days-natural f))
                                  :autocomplete "off"
                                  :class "food-edit-input food-expires-input")
                          (:a :href (escape-string
                                     (construct-url (url 'remove-food)
                                                    :name (food-name f)))
                              :class "remove-link" "remove")
                          (hidden-submit "rename"))))))
      (unless (pg-session-value 'anonymous-account)
        (htm
         (:section
          :class "options-container"
          (:form :name "set-email-digest" :action (url 'email-digest) :method "post"
                 (:input :type "checkbox" :name "enable-digest" :id "enable-email-digest"
                         :checked (email-digest (current-user)))
                 (:label :for "enable-email-digest" "receive daily email digest")
                 (fmt "~A" "&nbsp")
                 (:input :type "submit" :value "update")))
         (:section
          :class "options-container"
          (:form :name "set-tracking" :action (url 'tracking) :method "post"
                 (:input :type "checkbox" :name "tracking" :id "tracking"
                         :checked (pg-session-value 'tracking))
                 (:label :for "tracking" "allow google analytics")
                 (fmt "~A" "&nbsp")
                 (:input :type "submit" :value "update")))))
      (js
       (defsetf visible (el) (visible)
         `(setf (@ ,el style display)
                (if ,visible "block" "none")))
       (let ((filter ($id "filter")))
         (setf (@ filter onkeyup)
               (lambda ()
                 (loop
                    with search-for = (chain filter value (to-lower-case))
                    for e in ($class "food-name-input") do
                      (setf (visible (@ e parent-element parent-element))
                            (>= (chain e value (to-lower-case) (index-of search-for))
                                0)))
                 (loop for e in ($class "time-header") do (setf (visible e) f))
                 (loop
                    for e in (@ ($id "food-list-container") children)
                    for header = e then (if (=== (@ e class-name) "time-header") e header) do
                      (when (=== (@ e style display) "block")
                        (setf (visible header) t))))))))))


(defun ensure-valid-parameters (name expires)
  (let ((max-expires-length 10000) ; 30 years
        (min-expires-length -365) ; 1 year, cheese maybe?
        (max-name-length 150))
    (cond ((= (length name) 0)
           (redirect (construct-url (url 'app) :missing-name "true")))
          ((not expires)
           (redirect (construct-url (url 'app) :invalid-expires "true")))
          ((> (length name) max-name-length)
           (redirect (construct-url (url 'app) :name-too-long "true")))
          ((> expires max-expires-length)
           (redirect (url 'app)))
          ((< expires min-expires-length)
           (redirect (url 'app)))))
  t)

(defun food-exists-p (user-id name)
  (query (:select 'name :from 'food :where (:and (:= 'name '$1) (:= 'user-id '$2)))
         name user-id))

(defun ensure-food-exists (user-id name)
  (when (not (food-exists-p user-id name))
    (redirect (url 'app))))

(define-easy-handler/db
    (add-food-handler :uri (url 'add-food)) (name expires)
  (let ((parsed-expires (parse-expiration expires)))
    (ensure-valid-parameters name parsed-expires)
    (upsert-dao (make-food (pg-session-value 'user-id) name parsed-expires)))
  (redirect (url 'app)))

(define-easy-handler/db
  (remove-food-handler :uri (url 'remove-food)) (name)
  (progn
    (ensure-food-exists (pg-session-value 'user-id) name)
    (let ((f (get-dao 'food (pg-session-value 'user-id) name)))
      (delete-dao f)
      (redirect (construct-url (url 'app)
                               :undo "add" :undo-name (food-name f)
                               :undo-expires (food-expires-in-days f))))))
(define-easy-handler/db
    (rename-food-handler :uri (url 'rename-food))
    (name old-name numeric-expires initial-expires-text expires-in)
  ; some values are fuzzy so if they have only changed the text then
  ; we want to make sure to keep the exact expiration from changing.
  (let ((parsed-expires
         (if (string= initial-expires-text expires-in)
             (timestamp->days (parse-integer numeric-expires))
             (parse-expiration expires-in))))
    (ensure-valid-parameters name parsed-expires)
    (ensure-valid-parameters old-name 1)
    (ensure-food-exists (pg-session-value 'user-id) old-name)
    (delete-dao (get-dao 'food (pg-session-value 'user-id) old-name))
    (upsert-dao (make-food (pg-session-value 'user-id) name parsed-expires)))
  (redirect (construct-url (url 'app) :undo "rename" :undo-orig-name old-name
                           :undo-cur-name name
                           :undo-expires numeric-expires)))

(define-easy-handler/db
    (set-email-digest-handler :uri (url 'email-digest))
    ((enable-digest :parameter-type 'boolean))
  (let ((u (current-user)))
    (setf (email-digest u) enable-digest)
    (save-dao/transaction u))
  (redirect (url 'app)))

(define-easy-handler/db
    (set-tracking-handler :uri (url 'tracking))
    ((tracking :parameter-type 'boolean))
  (setf (pg-session-value 'tracking) tracking)
  (redirect (url 'app)))

(define-easy-handler/db
    (disable-email-digest-by-id-handler :uri (url 'disable-email-digest-by-id))
    ((id :parameter-type 'integer))
  (standard-page
    (if id
        (handler-case
            (let ((u (get-dao 'user (get-user-email-by-id id) id)))
              (setf (email-digest u) nil)
              (save-dao/transaction u)
              (log:info "User ~A unsubscribed from email digest." id)
              (htm (:p "You have been unsubscribed successfully.")))
          (user-id-not-found-error ()
            (log:error "User id ~A not found when unsubscribing from email digest." id)
            (htm (:p "Error. Could not unsubscribe."))))
        (progn (log:error "Invalid user id type.")
               (htm (:p "Error. Could not unsubscribe."))))))

(define-easy-handler/db
    (demo-handler :uri (url 'demo))
    ()
  (let ((id
         (query (:select
                 (:raw "nextval(pg_get_serial_sequence('user_accounts', 'user_id'))"))
                :single)))
    (unless (null *session*) (redirect (url 'app)))
    (create-user-session id :anonymous-account t)
    (upsert-dao (make-food (pg-session-value 'user-id) "Bagels" 6))
    (upsert-dao (make-food (pg-session-value 'user-id) "Milk" 1))
    (upsert-dao (make-food (pg-session-value 'user-id) "Cheese" 45)))
  (redirect (url 'app)))

;;; CSS ;;;
(defparameter
 *main-css*
 (cl-css:inline-css
  (let ((odd-row-color "#ccc")
        (site-bar-color "black")
        (medium-blue "#428bca")
        (white "#ffffff")
        (black "black"))
    `((body :margin-left auto :margin-right auto :margin-top 0px :padding 0px
            :max-width 530px)
      (a :color black)
      (.navbar :border-top ,(++ "4px solid " site-bar-color) :margin-bottom 10px
               :padding-top 6px)
      (.footerbar :border-bottom ,(++ "2px solid " site-bar-color))
      ("#food-list-container" :width 342px)
      (.food-edit-input :border ,(++ "1px solid " white) :padding 2px)
      (.food-expires-input :width 100px)
      (.food-name-input :width 140px)
      (".food-edit-row:nth-of-type(even)" :background-color ,odd-row-color)
      (".food-edit-row:nth-of-type(even) input" :background-color ,odd-row-color
                                                :border-color ,odd-row-color)
      (.time-header :background-color ,medium-blue :font-size 20px :color ,white
                    :padding 4px
                    :font-family "'Helvetica Neue', Helvetica, Arial, sans-serif")
      (.add-food-section :margin-bottom 6px :line-height 30px)
      (".add-food-section form input" :padding 2px :margin-right 4px :margin-left 0px)
      (".food-edit-input:hover" :border ,(++ "1px solid " black))
      (".food-edit-row:nth-of-type(even) input:hover" :border-color ,black)
      (".options-container" :margin-top 8px :margin-bottom 4px)))))


(add-urls (url 'css-main-root)
          `(css-main ,(++ (princ-to-string (sxhash *main-css*)) ".css")))

(define-easy-handler
    (main-css :uri (url 'css-main)) ()
  (setf (hunchentoot:content-type* hunchentoot:*reply*) "text/css")
  *main-css*)
