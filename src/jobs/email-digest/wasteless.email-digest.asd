(defpackage :wasteless.email-digest-system
  (:use :cl :asdf))
(in-package :wasteless.email-digest-system)

(defsystem :wasteless.email-digest
  :serial t
  :description "Describe wasteless here"
  :author "Thomas Hintz"
  :license "BSD"
  :depends-on (#:cl-who
               #:wasteless.date-time-utils
               #:postmodern
               #:cl-sendgrid
               #:wasteless.common
               #:wasteless.food
               #:wasteless.user
               #:log4cl
               #:trivial-backtrace)
  :components ((:file "package")
               (:file "email-digest")))
