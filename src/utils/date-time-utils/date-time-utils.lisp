(in-package :wasteless.date-time-utils)

(defconstant seconds/day (* 60 60 24))

(defun string->integer (s)
  (let ((n (read-from-string s)))
    (when (integerp n) n)))

(defun timestamp->days (timestamp)
  (round (+ (/ (- timestamp
                  (get-universal-time))
               seconds/day)
            0.0)))

(defun days->timestamp (days)
  (+ (get-universal-time) (* seconds/day days)))

(defun plain-days (s)
  "Returns an integer iff the string contains only an integer.
   Returns nil otherwise."
  (handler-case (parse-integer s)
        (condition () nil)))

(define-condition unparseable-expiration-date-error (error)
    ((unparseable-expiration-date-text :initarg :text
                                       :reader unparseable-expiration-date-text)))

(defmacro hashlist (list table)
  `(dolist (item ,list)
     (setf (gethash (car item) ,table) (cdr item))))

(defvar *weekday-strings* (make-hash-table :test #'equal
                                           :size 23))

(hashlist '(("monday" . 0)    ("mon" . 0)
	    ("tuesday" . 1)   ("tues" . 1)   ("tue" . 1)
	    ("wednesday" . 2) ("wednes" . 2) ("wed" . 2)
	    ("thursday" . 3)  ("thurs" . 3)  ("thu" . 3)
	    ("friday" . 4)    ("fri" . 4)
	    ("saturday" . 5)  ("sat" . 5)
	    ("sunday" . 6)    ("sun" . 6))
	  *weekday-strings*)

(defun day-of-week (string)
  (gethash string *weekday-strings*))

(defun day-of-week->days (day-of-week-string &key (timestamp (get-universal-time)))
  (let ((current
         (multiple-value-bind
               (second minute hour day-of-month month year day-of-week daylight-p zone)
             (decode-universal-time timestamp)
           (declare (ignore second minute hour day-of-month month year daylight-p zone))
           day-of-week))
        (input (day-of-week day-of-week-string)))
    (if (< current input)
        (- input current)
        (- (+ input 7) current))))

(defun date-of-expiration (universal-time)
  (multiple-value-bind
        (second minute hour day-of-month month year day-of-week daylight-p zone)
      (decode-universal-time universal-time)
    (declare (ignore second minute hour day-of-week daylight-p zone))
    (format nil "~A ~D, ~D" (net.telent.date:monthname nil month nil nil)
            day-of-month year)))

(defun parse-expiration (s &key error-on-fail)
  (let ((num (handler-case
                 (cond ((plain-days s)
                        (plain-days s))
                       ((string= s "yesterday") -1)
                       ((string= s "today") 0)
                       ((string= s "tomorrow") 1)
                       ((or (search " day" s) (search " days" s))
                        ; The space before day is important to prevent
                        ; having days of week like monday be a false
                        ; positive.
                        (string->integer s))
                       ((or (search " week" s) (search " weeks" s))
                        (* (string->integer s) 7))
                       ((or (search " month" s) (search " months" s))
                        (* (string->integer s) 30))
                       ((or (search " year" s) (search " years" s))
                        (* (string->integer s) 365))
                       ((day-of-week s)
                        (day-of-week->days s))
                       (t (timestamp->days (parse-time s))))
               (error () nil))))
    (if (or num (not error-on-fail))
        num
        (error 'unparseable-expiration-date-error :text s))))

(defun days-natural (days)
  "Convert days into a natural reading like 1 -> 'tomorrow'."
  (cond
    ((= days -1) "yesterday")
    ((= days 0) "today")
    ((= days 1) "tomorrow")
    ((and (< days 7) (> days -7))
     (multiple-value-bind
           (second minute hour day-of-month month year day-of-week daylight-p zone)
         (decode-universal-time (days->timestamp days))
       (declare (ignore second minute hour day-of-month month year daylight-p zone))
       (format nil "~:[last ~A~;~A~]"
               (> days 0) (net.telent.date:dayname nil day-of-week nil nil))))
    ((and (< days 14) (> days -14))
     (format nil "~D day~:p~@[ ago~]" (abs days) (< days 0)))
    ((and (< days 30) (> days -30))
     (format nil "~D week~:p~@[ ago~]" (round (/ (abs days) 7)) (< days 0)))
    ((and (< days 340) (> days -340))
     (format nil "~D month~:p~@[ ago~]"(round (/ (abs days) 30)) (< days 0)))
    (t (format nil "~D year~:p~@[ ago~]" (round (/ (abs days) 365)) (< days 0)))))
