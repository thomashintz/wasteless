(defpackage #:wasteless.food
  (:use #:cl #:postmodern #:wasteless.date-time-utils)
  (:nicknames #:w.food)
  (:export #:food
           #:food-user-id
           #:food-name
           #:food-expires
           #:food-expires-in-days
           #:food-days-natural
           #:make-food))
(in-package :wasteless.food)

