(defpackage #:wasteless.user
  (:use #:cl #:postmodern #:cl-password #:wasteless.common)
  (:nicknames #:w.user)
  (:export #:password-string->plist
           #:user
           #:user-id
           #:user-email
           #:user-password
           #:email-digest
           #:make-user
           #:account-exists-p
           #:account-exists-error
           #:password-too-short-error
           #:password-too-long-error
           #:email-too-long-error
           #:user-account-error
           #:user-account-error-message
           #:add-user-account))
(in-package :wasteless.user)

