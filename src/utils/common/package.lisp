(defpackage #:wasteless.common
  (:use #:cl)
  (:export #:conf #:load-config #:with-gensyms #:do-tuples/o #:map0-n
           #:between #:wasteless-src-path #:w/c #:with-shadow #:fun-orig
           #:url #:add-urls))

