(defpackage #:wasteless.date-time-utils
  (:use #:cl #:net.telent.date)
  (:nicknames #:w.date-time-utils)
  (:export #:timestamp->days
           #:days->timestamp
           #:unparseable-expiration-date-erro
           #:parse-expiration
           #:days-natural))

