(defpackage :wasteless.web-system
  (:use :cl :asdf))
(in-package :wasteless.web-system)

(defsystem :wasteless.web
  :serial t
  :description "Describe wasteless here"
  :author "Thomas Hintz"
  :license "BSD"
  :depends-on (#:cl-who
               #:hunchentoot
               #:cl-password
               #:wasteless.date-time-utils
               #:postmodern
               #:cl-init
               #:cl-ppcre
               #:wasteless.food
               #:wasteless.user
               #:cl-css
               #:log4cl
               #:trivial-backtrace
               #:swank
               #:anaphora
               #:pg-sessions
               #:parenscript
               #:wasteless.common
               #:net-telent-date)
  :components ((:file "package")
               (:file "web-macros")
               (:file "wasteless-web")))
