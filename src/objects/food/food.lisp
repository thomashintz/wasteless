(in-package :wasteless.food)

(defclass food ()
  ((user-id :col-type smallint :initarg :user-id :reader food-user-id)
   (name :col-type string :initarg :name :accessor food-name)
   (expires :col-type bigint :initarg :expires :accessor food-expires))
  (:metaclass dao-class)
  (:keys user-id name))

(defgeneric food-expires-in-days (food)
  (:documentation "Days until food expires."))

(defgeneric food-date-of-expiration (food)
  (:documentation "Like December fifth, 2013."))

(defgeneric food-days-natural (food)
  (:documentation "Like 'yesterday', 1 month, etc. For easy relative reading."))

(defmethod food-expires-in-days ((f food))
  (timestamp->days (food-expires f)))

(defmethod food-date-of-expiration ((f food))
  (date-of-expiration (food-expires f)))

(defmethod food-days-natural ((f food))
  (days-natural (food-expires-in-days f)))

(defun make-food (user-id name expires-in-days)
  (assert (numberp expires-in-days) (expires-in-days)
          "must be a number: ~S" expires-in-days)
  (make-instance 'food :user-id user-id :name name
                 :expires (days->timestamp expires-in-days)))
