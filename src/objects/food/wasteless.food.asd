(defpackage :wasteless.food-system
  (:use :cl :asdf))
(in-package :wasteless.food-system)

(defsystem :wasteless.food
  :serial t
  :depends-on (#:postmodern
               #:wasteless.date-time-utils)
  :components ((:file "package")
               (:file "food")))
