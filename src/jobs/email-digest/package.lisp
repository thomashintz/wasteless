(defpackage #:wasteless.email-digest
  (:nicknames :w.email-digest)
  (:use #:cl
        #:wasteless.common
        #:postmodern
        #:cl-who
        #:wasteless.user
        #:wasteless.food
        #:wasteless.date-time-utils))
(in-package :wasteless.email-digest)

