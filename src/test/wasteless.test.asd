(defpackage :wasteless.test-system
  (:use :cl :asdf))
(in-package :wasteless.test-system)

(defsystem :wasteless.test
  :serial t
  :depends-on (#:wasteless.web
               #:postmodern
               #:drakma
               #:hunchentoot
               #:cl-ppcre
               #:puri
               #:wasteless.common
               #:wasteless.date-time-utils
               #:fiveam)
  :components ((:file "package")
               (:file "test-wasteless")))
