(in-package :wasteless.web)

(defmacro js (&body body)
  `(htm (:script :type "text/javascript" (fmt "~A" (ps ,@body)))))

(defpsmacro $id (id)
  `(chain document (get-element-by-id ,id)))

(defpsmacro $class (class)
  `(chain document (get-elements-by-class-name ,class)))

(defmacro html-to-str (&body body)
  `(with-html-output (*standard-output* nil) ,@body))
