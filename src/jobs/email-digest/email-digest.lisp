(in-package :wasteless.email-digest)

(load-config)
(log:config :info)
(log:config :daily (conf 'email-digest-log-path))

(defparameter *db* nil)

(defun within-food-expiration-range (previous-food current-food begin end)
  (and (< (food-expires-in-days previous-food) begin)
       (between (food-expires-in-days current-food) begin end)))

(defmacro unsubscribe-link (user-id)
  `(htm (:p "You are receiving this email because you enabled email digests at "
            (:a :href "http://wasteless.io" "wasteless.io")
            ". To unsubscribe from these emails "
            (:a :href (format nil "https://a.wasteless.io~A?id=~A"
                              (url 'disable-email-digest-by-id) ,user-id)
                "click here") ".")))

(defun food-digest-table (user-id)
  (with-html-output-to-string (s)
    (let ((foods (w/c (select-dao 'food (:= 'user-id user-id) 'expires))))
      (unsubscribe-link user-id)
      (do-tuples/o (prev current) (cons (make-food -1 "" -10000) foods) ; dummy first food
        (cond ((within-food-expiration-range prev current 0 7)
               (htm (:h1 "days")))
              ((within-food-expiration-range prev current 7 30)
               (htm (:h1 "weeks")))
              ((within-food-expiration-range prev current 30 365)
               (htm (:h1 "months")))
              ((within-food-expiration-range prev current 365 10000)
               (htm (:h1 "years"))))
        (when (>= (food-expires-in-days current) 0)
          (htm (:p (fmt "~A - ~A" (food-name current) (food-days-natural current))))))
      (unsubscribe-link user-id))))

(defun send-email-digests ()
  (let ((user-ids (w/c
                    (query
                     (:select 'user-id :from 'user-accounts :where (:= 'email-digest "true"))
                     :column))))
    (loop for id in user-ids
          for email = (w/c
                        (query (:select 'email :from 'user-accounts :where (:= 'user-id '$1))
                               id :single)) do
         (log:info "sending email to: ~A" email)
         (handler-case
             (cl-sendgrid:send-email
              :api-user "t@thintz.com" :api-key (conf 'send-grid-password)
              :subject "Daily food digest - wasteless.io"
              :to email
              :from "email-digest@wasteless.io"
              :from-name "Wasteless.io"
              :reply-to "t@thintz.com"
              :html (food-digest-table id))
           (cl-sendgrid:sendgrid-error (c)
             (log:error "sendgrid error for ~A, ~A" email
                        (cl-sendgrid:sendgrid-error-message c)))))))

(defun run ()
  (send-email-digests)
  (ccl:quit))

(run)
