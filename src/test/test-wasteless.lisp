(in-package :wasteless.test)

(load-config)

(defun connect-db ()
  (postmodern:connect-toplevel (conf 'db-database) (conf 'db-user) (conf 'db-password) (conf 'db-host)))

(unless (and postmodern:*database* (postmodern:connected-p postmodern:*database*))
  (connect-db))

(def-suite :suite-web)
(in-suite :suite-web)

(def-fixture db-fixtures ()
  (postmodern:with-transaction (transaction)
    (&body)
    (postmodern:abort-transaction transaction)))

(defmacro db-test (test-name &body body)
  `(test ,test-name
     (with-fixture db-fixtures ()
       ,@body)))

(defmacro with-test-user (user &body body)
  (with-gensyms (email password)
    `(let ((,email "tkdicsidkamdkfi@dkickeidk.com")
           (,password "foofoofoo"))
       (if (w.web::account-exists-p ,email)
           (fail "test account already exists")
           (progn
             (let ((,user (w.web::add-user-account ,email ,password)))
               ,@body))))))

(unwind-protect
     (postmodern:execute (:raw (++ "delete from user_accounts where email='tkdicsidkamdkfi@dkickeidk.com'")))
  nil)

(db-test test-user-accounts
  (let ((e "tkdicsidkamdkfi@dkickeidk.com")
        (p "foo"))
    (if (w.web::account-exists-p e)
        (fail "test account already exists")
        (progn
          (is-true (w.user:add-user-account e p))
          (is-true (w.web::verify-user-credentials e p))
          (is-false (w.user:account-exists-p "dkcikdiakdidkgidkdicid@kikckdidic.com"))
          (signals w.web::invalid-user-credentials-error
            (w.web::verify-user-credentials e "asdf"))
          (signals w.user:account-exists-error (w.user:add-user-account e p))))))

(db-test test-foods
  (with-test-user user
    (let* ((expires "2")
           (f (w.food:make-food (w.user:user-id user) "pizza"
                                    (w.date-time-utils:parse-expiration expires))))
      (postmodern:upsert-dao f)
      (pass "food creation and insertion successful")
      (is (= (w.date-time-utils::string->integer expires)
             (w.food:food-expires-in-days f))))))

(test test-expirations
  (is (= (w.date-time-utils::string->integer "5")
         5))
  (is (equal (w.date-time-utils::string->integer "asdf")
             nil))
  (is (= (w.date-time-utils::plain-days "1")
         1))
  (is (equal (w.date-time-utils::plain-days "asdf")
             nil))
  (let* ((days 5)
         (days-timestamp (w.date-time-utils:days->timestamp days)))
    (is (= (w.date-time-utils::timestamp->days days-timestamp)
           days)))
  (let ((timestamp 3595623655))
    (is (= (w.date-time-utils::day-of-week->days "wed" :timestamp timestamp)
           2))
    (is (= (w.date-time-utils::day-of-week->days "sunday" :timestamp timestamp)
           6))
    (is (string= (w.date-time-utils::date-of-expiration timestamp)
                 "December 9, 2013")))
  (is (= (w.date-time-utils:parse-expiration "yesterday")
         -1))
  (is (= (w.date-time-utils:parse-expiration "5 days")
         5))
  (is (= (w.date-time-utils:parse-expiration "2 weeks")
         14))
  (is (= (w.date-time-utils:parse-expiration "5 months")
         150))
  (is (= (w.date-time-utils:parse-expiration "5 days")
         5)))

(test test-uri-root
  (is (string= "/home"
               (w.web::uri-root "/home?foo=bar&baz")))
  (is (string= "/home/asdf/"
               (w.web::uri-root "/home/asdf/")))
  (is (string= "/"
               (w.web::uri-root "/")))
  (is (string= "/home"
               (w.web::uri-root "/home?foo/"))))

(test test-expiration-dates
  (is-true (w.date-time-utils:parse-expiration "tuesday"))
  (is-true (w.date-time-utils:parse-expiration "wed"))
  (is (= 1 (w.date-time-utils:parse-expiration "1")))
  (is-false (w.date-time-utils:parse-expiration "1 million"))
  (is-false (w.date-time-utils:parse-expiration "asdf dk 1 dkdidi"))
  (is (= 9 (w.date-time-utils:parse-expiration "9 days")))
  (is (= 9 (w.date-time-utils:parse-expiration "9 days of food")))
  (is (= 30 (w.date-time-utils:parse-expiration "1 month")))
  (is (= 300 (w.date-time-utils:parse-expiration "10 months")))
  (is-true (w.date-time-utils:parse-expiration "10 dec"))
  (is-true (w.date-time-utils:parse-expiration "january 10"))
  (is-true (w.date-time-utils:parse-expiration "jan 10"))
  (is-true (w.date-time-utils:parse-expiration "jan 10, 2020"))
  (is-false (w.date-time-utils:parse-expiration "jan 10 dkdidk")))

(test test-valid-email
  (is-true (w.web::valid-email-p "t@f"))
  (is-true (w.web::valid-email-p "t@f.com"))
  (is-true (w.web::valid-email-p "t.foo@f.goo"))
  (is-false (w.web::valid-email-p "tf"))
  (is-false (w.web::valid-email-p "t.f"))
  (is-false (w.web::valid-email-p "t@"))
; (is-false (w.web::valid-email-p "@f")) should fail, adds complexity though
  (is-false (w.web::valid-email-p "t@f@d.com")))

(test test-construct-url
  (is (string= "/add?name=red%20berries%26%20%3B&expires=1029394&"
               (w.web::construct-url "/add" :name "red berries& ;" :expires 1029394))))

(defun strip-url-params (url)
  (car (cl-ppcre:split "\\?" url)))

(defparameter *base-url* (format nil "http://localhost:~A" (conf 'httpd-port)))
(defparameter *last-reply* nil)
(defparameter *cookie-jar* (make-instance 'cookie-jar))

(defclass http-reply ()
  ((body :initarg :body)
   (status-code :initarg :status-code)
   (headers :initarg :headers)
   (uri :initarg :uri)
   (url :initarg :url)
   (url-no-params :initarg :url-no-params)
   (stream :initarg :stream)
   (must-close :initarg :must-close)
   (reason-phrase :initarg :reason-phrase)))

(defun test-http-request (url &key (method :get) parameters cookie-jar)
  (multiple-value-bind
        (body status-code headers uri stream must-close reason-phrase)
      (http-request (++ *base-url* url) :method method :parameters parameters
                    :cookie-jar cookie-jar)
    (setf *last-reply*
          (make-instance 'http-reply
                         :body body
                         :status-code status-code
                         :headers headers
                         :uri uri
                         :must-close must-close
                         :stream stream
                         :reason-phrase reason-phrase
                         :url (cadr (cl-ppcre:split *base-url*
                                                    (with-output-to-string (s)
                                                      (puri:render-uri uri s))))))))

(defun http-get (url) (test-http-request url :cookie-jar *cookie-jar*))

(defun http-post (url parameters)
  (test-http-request url :method :post :parameters parameters :cookie-jar *cookie-jar*))

(defun assert-200 ()
  (is (= (slot-value *last-reply* 'status-code) hunchentoot:+http-ok+)))
(defun assert-500 ()
  (is (= (slot-value *last-reply* 'status-code) hunchentoot:+http-ok+)))
(defun assert-base-url (url)
  (is (string= (strip-url-params (slot-value *last-reply* 'url)) url)))

(defun empty-cookie-jar ()
  (setf *cookie-jar* (make-instance 'cookie-jar)))

(defun url (key) (w.web::url (intern (symbol-name key) 'wasteless.web)))

(run! 'test-pages)
(test test-pages
  (w.web::start-server)
  (let ((email "test-suite-test-user@test.com")
        (password "asdfasdf")
        (bagels (w.food:make-food 0 "bagels" 14))
        (new-food-name "whole wheat bagels"))
    (unwind-protect
         (postmodern:execute (:raw (++ "delete from user_accounts where email='" email "'")))
      nil)
    (unwind-protect
         (progn
           (sleep 2) ; give the server a second to start up

           (http-get (url 'sign-in))
           (assert-200)

           (http-post (url 'sign-in-trampoline)
                      `(("email" . ,email) ("password" . ,password)))
           (assert-200)
           (assert-base-url (url 'app))
           (is (= 1 (length (cookie-jar-cookies *cookie-jar*))))
           (is (string= "hunchentoot-session"
                        (cookie-name (car (cookie-jar-cookies *cookie-jar*)))))

           (http-post (url 'email-digest) '(("enable-digest" . "true")))
           (assert-200)
           (assert-base-url (url 'app))

           (http-get (url 'app))
           (assert-200)
           (assert-base-url (url 'app))

           (http-post (url 'add-food)
                      `(("name" . ,(w.food:food-name bagels))
                        ("expires" . ,(++ (princ-to-string
                                           (w.food:food-expires-in-days bagels)) " days"))))
           (assert-200)
           (assert-base-url (url 'app))

           (http-get (url 'sign-out))
           (assert-200)
           (assert-base-url (url 'sign-in))

           (http-post (url 'email-digest) '(("enable-digest" . "true")))
           (assert-500) ; we can't do this if we are signed out

           (http-post (url 'sign-in-trampoline)
                      `(("email" . ,email) ("password" . ,password) ("type" . "sign-in")))
           (assert-200)
           (assert-base-url (url 'app))

           (http-post (url 'rename-food)
                      `(("name" . ,new-food-name)
                        ("old-name" . ,(w.food:food-name bagels))
                        ("numeric-expires" . ,(w.food:food-expires bagels))
                        ("initial-expires-text" . ,(w.food:food-days-natural bagels))
                        ("expires-in" . "tomorrow")))
           (assert-200)
           (assert-base-url (url 'app))

           (http-post (url 'remove-food) `(("name" . ,new-food-name)))
           (assert-200)
           (assert-base-url (url 'app)))
      (unwind-protect
           (postmodern:execute
            (:raw (++ "delete from user_accounts where email='" email "'")))
        (w.web::stop-server)))))

(run!)
