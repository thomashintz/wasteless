(defpackage #:wasteless.web
  (:nicknames :w.web)
  (:use #:cl
        #:wasteless.common
        #:postmodern
        #:cl-password
        #:hunchentoot
        #:cl-who
        #:wasteless.user
        #:wasteless.food
        #:wasteless.date-time-utils
        #:anaphora
        #:pg-sessions
        #:parenscript
        #:net.telent.date)
  (:shadow :redirect))
(in-package :wasteless.web)

