(ql:quickload "trivial-backtrace")

(ql:quickload "cl-init")
(ql:quickload "wasteless.web")

(defun run-server ()
    (handler-case
        (progn
          (setf cl-init:*start-thunk* #'(lambda ()
                                          (wasteless.web::start-server)))
          (setf cl-init:*stop-thunk* #'(lambda ()
                                         (wasteless.web::stop-server)))
          (cl-init:run))
      (error (c)
        (princ (trivial-backtrace:print-backtrace c :output nil))
        (quit))))
