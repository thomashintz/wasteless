(in-package :wasteless.user)

(defun password-string->plist (password-string)
  (with-input-from-string (s password-string)
    (read s)))

(defun password-plist->string (password-plist)
  (with-output-to-string (s)
     (write password-plist :stream s)))

(defclass user ()
  ((user-id :col-type serial :initarg :user-id :reader user-id)
   (email :col-type string :initarg :email :reader user-email)
   (password :col-type string :initarg :password :accessor user-password)
   (email-digest :col-type boolean :initarg :email-digest :accessor email-digest))
  (:metaclass dao-class)
  (:keys email user-id)
  (:table-name "user-accounts"))

(defun make-user (email password &key hash-password)
  (make-instance
   'user
   :email-digest t
   :email email
   :password (if hash-password
                 (password-plist->string (hash-password password :scrypt))
                 password)))

(defun account-exists-p (email)
  (query (:select 'email :from 'user-accounts :where (:= 'email '$1)) email))

(define-condition user-account-error (error)
    ((error-message :initarg :error-message :reader user-account-error-message)))

(define-condition account-exists-error (user-account-error)
    ((account-exists-email :initarg :email :reader account-exists-email)))

(define-condition password-too-short-error (user-account-error) ())
(define-condition password-too-long-error (user-account-error) ())
(define-condition email-too-long-error (user-account-error) ())

(defun add-user-account (email password &key user-id)
  (cond ((< (length password) 6)
         (error 'password-too-short-error :error-message "password too short"))
        ((> (length password) 100)
         (error 'password-too-long-error :error-message "password too long"))
        ((> (length email) 100)
         (error 'email-too-long-error :error-message "email too long")))
  (if (and (not user-id) (account-exists-p email))
      (error 'account-exists-error :email email :error-message "account already exists")
      (apply #'make-dao
             (append `(user
                       :email-digest t :email ,email
                       :password ,(password-plist->string (hash-password password :scrypt)))
                     (when user-id `(:user-id ,user-id))))))
